serviceName = "BestBuy-Services";
integrationObj = KNYMobileFabric.getIntegrationService(serviceName);

define({ 

  onNavigate(id){
    this.view.HeaderMenu.btnBack.isVisible=false;
    this.view.HeaderMenu.btnBack.onClick=this.btnBack;
    this.view.segCategories.onRowClick=this.categorySelected;
    this.getCategories();


  },

  getCategories :function(id){

    operationName =  "getCategories";
    data= {"id": id}; 
    headers= {};
    integrationObj.invokeOperation(operationName, headers, data, 
                                   this.operationSuccess, 
                                   this.operationFailure);


  },

  operationSuccess :function(res){
   categories=res.category;
    categories.map((item)=>{

      var categoryDetail={};

      categoryDetail.name=item.name;
      categoryDetail.id=item.id;

       return  categoryDetail;

    });

    cache.push(categories);
    this.view.segCategories.widgetDataMap={lblCategoriesTitle:"name"};
    this.view.segCategories.setData(categories);
      
      
    

  

  },


  operationFailure :function(res){
    alert(res);

  },


  categorySelected :function(){

    this.view.HeaderMenu.btnBack.isVisible=true;
    var segmentCategory=this.view.segCategories.selectedRowItems[0];
        
      id=segmentCategory.id;
      var title=segmentCategory.name;
      this.view.lblTitle.text=`Home → ${title}`;
       count  ++;
    alert(count);

    
    if(count ==3){
      
     
      var ntf = new kony.mvc.Navigation("frmProductList");
    ntf.navigate(id);
    }
    

      this.getCategories(id);



  },


  btnBack :function(){
    
    cache.pop();
     count --;
    alert(count);
    index= `${cache.length}`;

    if(   index  ==1 ){

      this.view.HeaderMenu.btnBack.isVisible=false;

      this.view.segCategories.widgetDataMap={lblCategoriesTitle:"name"};
      this.view.segCategories.setData(cache[cache.length -1]);


    } else{

      this.view.segCategories.widgetDataMap={lblCategoriesTitle:"name"};
      this.view.segCategories.setData(cache[cache.length -1]);
    }


  }



});
