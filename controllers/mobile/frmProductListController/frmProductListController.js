

define({ 

  onNavigate: function(id){
    this.view.HeaderMenu.btnBack.onClick=this.previousPage;
    this.view.segProductList.onRowClick=this.productSelected;
 
this.getProducts(id);
    
  },
  
  
 getProducts : function (id)  {
    
    {operationName =  "getProducts";
    data= {"catID": id,"page": "1"};
    headers= {};
    integrationObj.invokeOperation(operationName, headers,
                                   data, this.operationSuccess, 
                                   this.operationFailure);
    }
  },
  
  
  
   operationSuccess : function(res){
	var products=res.products;
     


     products.map((item)=>{
       
       var product={};

       product.name=item.name;
       product.salePrice=item.salePrice;
       product.customerReviewAverage=item.customerReviewAverage;
       product.image =item.image;
       product.sku=item.sku;
       product.images =item.images;
       product.shortDescription=item.shortDescription;

//       

       
       
       return  product;
       
       
     });
     
     

     
     

          this.view.segProductList.widgetDataMap={lblProductsTitle:"name",
                                         lblProductPrice:"salePrice",
                                         lblAvgUserRating:"customerReviewAverage",
                                                  imgProduct:"image"
                                         
                                         
                                         };
         this.view.segProductList.setData(products);

          


     
     
     

     
},
  
  
 operationFailure : function(res){
alert(res);
},
  
  previousPage: function(){
    
     var ntf = new kony.mvc.Navigation("frmHome");
    ntf.navigate();
    
  },
  
  
  productSelected :function(){
    
   productDetails=this.view.segProductList.selectedRowItems[0];
    var ntf = new kony.mvc.Navigation("frmProductDetails");
    ntf.navigate(productDetails);
    
    
    
  }
  
   
  
 });